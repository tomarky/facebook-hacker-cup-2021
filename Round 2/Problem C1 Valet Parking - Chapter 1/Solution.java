import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        int[] RCK = readInts();
        int R = RCK[0];
        int C = RCK[1];
        int K = RCK[2] - 1;
        
        byte[][] garage = new byte[R][];
        int[][] sum1 = new int[R+1][C];
        int[] simple = new int[R];
        
        for (int i = 0; i < R; i++)
        {
            garage[i] = in.readLine().getBytes();
            int s = 0;
            for (int e = 0; e < C; e++)
            {
                sum1[i+1][e] = sum1[i][e];
                if (garage[i][e] == 'X')
                {
                    sum1[i+1][e]++;
                    s++;
                }
            }
            simple[i] = s;
        }
        
        int[][] sum2 = new int[R+1][C];

        for (int i = R; i > 0; i--)
        {
            for (int e = 0; e < C; e++)
            {
                sum2[i-1][e] = sum2[i][e];
                if (garage[i-1][e] == 'X')
                {
                    sum2[i-1][e]++;
                }
            }
        }
        
        int answer = R * C;
        
        for (int i = 0; i < R; i++)
        {
            int s = 0;
            if (i < K)
            {
                s = K - i;
                for (int e = 0; e < C; e++)
                {
                    if (sum2[i+1][e] >= R - K || garage[i][e] == 'X')
                    {
                        s++;
                    }
                }
            }
            else if (i == K)
            {
                s = simple[i];
            }
            else if (i > K)
            {
                s = i - K;
                for (int e = 0; e < C; e++)
                {
                    if (sum1[i][e] >= K + 1 || garage[i][e] == 'X')
                    {
                        s++;
                    }
                }
            }
            answer = Math.min(answer, s);
        }        
        
        int s1 = R - K, s2 = K + 1;
        for (int e = 0; e < C; e++)
        {
            if (sum1[R][e] >= K+1)
            {
                s1++;
            }
            if (sum2[0][e] >= R - K)
            {
                s2++;
            }
        }
        answer = Math.min(answer, s1);
        answer = Math.min(answer, s2);
        
        out.println(answer);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}