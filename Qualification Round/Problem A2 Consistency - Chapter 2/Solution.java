import java.io.*;
import java.util.*;

class Solution
{
    static final int BIG = Integer.MAX_VALUE >> 2; 
    
    static void solve() throws Exception
    {
        // solution
        byte[] s = in.readLine().getBytes();
        int k = readInt();
        int[][] fw = new int[26][26];
        for (int[] row : fw) Arrays.fill(row, BIG);
        for (int i = 0; i < 26; i++) fw[i][i] = 0;
        for (int i = 0; i < k; i++) {
            byte[] ab = in.readLine().getBytes();
            int a = (int)ab[0] - 'A';
            int b = (int)ab[1] - 'A';
            fw[a][b] = 1;
        }
        for (int e = 0; e < 26; e++) {
            for (int i = 0; i < 26; i++) {
                for (int j = 0; j < 26; j++) {
                    if (fw[i][j] > fw[i][e] + fw[e][j]) {
                        fw[i][j] = fw[i][e] + fw[e][j];
                    }
                }
            }
        }
        int best = -1;
        for (int a = 0; a < 26; a++) {
            int cost = 0;
            for (byte b : s) {
                int x = fw[(int)b - 'A'][a];
                if (x == BIG) {
                    cost = -1;
                    break;
                }
                cost += x;
            }
            if (cost < 0) continue;
            if (best < 0 || cost < best) best = cost;
        }
        out.println(best);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}