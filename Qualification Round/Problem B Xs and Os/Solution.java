import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        int n = readInt();
        byte[][] cells = new byte[n][];
        for (int i = 0; i < n; i++) {
            cells[i] = in.readLine().getBytes();
        }
        int[][] memo = new int[n][2];
        int mincells = -1, sets = 0; 
        for (int i = 0; i < n; i++) {
            for (int k = 0; k < n; k++) {
                if (cells[i][k] == 'O') {
                    memo[i][0] = -1;
                    break;
                } else if (cells[i][k] == '.') {
                    memo[i][0]++;
                }
            }
            if (memo[i][0] >= 0 && (mincells < 0 || memo[i][0] < mincells)) {
                mincells = memo[i][0];
                sets = 1;
            } else if (mincells == memo[i][0]) {
                sets++;
            }
            for (int k = 0; k < n; k++) {
                if (cells[k][i] == 'O') {
                    memo[i][1] = -1;
                    break;
                } else if (cells[k][i] == '.') {
                    memo[i][1]++;
                }
            }
            if (memo[i][1] >= 0 && (mincells < 0 || memo[i][1] < mincells)) {
                mincells = memo[i][1];
                sets = 1;
            } else if (mincells == memo[i][1]) {
                sets++;
            }
        }
        
        if (mincells == 1) {
            sets = 0;
            for (int i = 0; i < n; i++) {
                if (memo[i][0] == 1) {
                    for (int k = 0; k < n; k++) {
                        if (cells[i][k] == '.') {
                            sets++;
                            cells[i][k] = 'Z';
                        }
                    }
                }
                if (memo[i][1] == 1) {
                    for (int k = 0; k < n; k++) {
                        if (cells[k][i] == '.') {
                            sets++;
                            cells[k][i] = 'Z';
                        }
                    }
                }
            }
        }
        
        if (mincells < 0) {
            out.println(IMPOSSIBLE);
        } else {
            out.println(mincells + " " + sets);
        }
    }

    static final String POSSIBLE = "Possible";
    static final String IMPOSSIBLE = "Impossible";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}