// FBHCのコンテストページに間違って別のコードおアップロードしたので
// こっちにverify通したコードをアップロードしておく…

import java.io.*;
import java.util.*;

class Solution
{
    static class Edge {
        final int id, from, to;
        Edge(int i, int f, int t) {
            id = i;
            from = f;
            to = t;
        }
    }
    
    static int K = 0;
    static long ans = 0L;
    static long[] cs = null;
    static ArrayList<ArrayList<Edge>> tree = null;
    static boolean[] used = null;
    static int[] visited = null;
    
    static void solve() throws Exception
    {
        // solution
        int[] nk = readInts();
        int n = nk[0];
        K = nk[1];
        cs = readLongs();
        tree = new ArrayList<>();
        for (int i = 0; i < n; i++) tree.add(new ArrayList<>());
        for (int i = 1; i < n; i++) {
            int[] ab = readInts();
            int a = ab[0]-1, b = ab[1]-1;
            tree.get(a).add(new Edge(i, a, b));
            tree.get(b).add(new Edge(i, b, a));
        }
        used = new boolean[n];
        visited = new int[n];
        visited[0] = 1;
        ans = cs[0];
        dfs(0, 0, cs[0]);
        out.println(ans);
    }
    
    static void dfs(int pos, int cost, long score) {
        if (pos == 0) {
            ans = Math.max(ans, score);
            if (cost >= K) return;
        }
        boolean found = false;
        for (Edge e : tree.get(pos)) {
            if (used[e.id]) continue;
            found = true;
            used[e.id] = true;
            if (visited[e.to] > 0) {
                dfs(e.to, cost, score);
            } else {
                visited[e.to]++;
                dfs(e.to, cost, score + cs[e.to]);
                visited[e.to]--;
            }
            used[e.id] = false;
        }
        if (found) return;
        if (cost >= K) return;
        dfs(0, cost + 1, score);
        for (int i = 1; i < tree.size(); i++) {
            if (visited[i] > 0) continue;
            if (tree.get(i).size() > 1) continue;
            visited[i]++;
            dfs(i, cost + 1, score + cs[i]);
            visited[i]--;
        }
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        long time0 = System.currentTimeMillis();
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
        
        long time1 = System.currentTimeMillis();
        System.err.println("time: " + (time1 - time0) + " msec");
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}