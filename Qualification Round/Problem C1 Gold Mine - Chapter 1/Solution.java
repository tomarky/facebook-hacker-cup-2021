import java.io.*;
import java.util.*;

class Solution
{
    static long dfs(
        ArrayList<ArrayList<Integer>> tree,
        long[] cs,
        boolean[] visited,
        int p
    ) {
        long sc = 0L;
        visited[p] = true;
        for (int e : tree.get(p)) {
            if (visited[e]) continue;
            long res = dfs(tree, cs, visited, e);
            sc = Math.max(sc, res);
        }
        visited[p] = false;
        return sc + cs[p];
    }
    static void solve() throws Exception
    {
        // solution
        int n = readInt();
        long[] cs = readLongs();
        ArrayList<ArrayList<Integer>> tree = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            tree.add(new ArrayList<>());
        }
        for (int i = 1; i < n; i++) {
            int[] e = readInts();
            int a = e[0]-1, b = e[1]-1;
            tree.get(a).add(b);
            tree.get(b).add(a);
        }
        boolean[] visited = new boolean[n];
        visited[0] = true;
        ArrayList<Long> list = new ArrayList<>();
        for (int p : tree.get(0)) {
            list.add(dfs(tree, cs, visited, p));
        }
        list.sort( (a, b) -> Long.compare(b, a) );
        long ans = cs[0];
        for (int i = 0; i < Math.min(2, list.size()); i++) {
            ans += list.get(i);
        }
        out.println(ans);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}