import java.io.*;
import java.util.*;

class Solution
{
    static boolean isVowel(byte c) {
        return c == 'A'
            || c == 'E'
            || c == 'I'
            || c == 'O'
            || c == 'U';
    }
    
    static void solve() throws Exception
    {
        // solution
        byte[] s = in.readLine().getBytes();
        
        int best = Integer.MAX_VALUE;
        
        for (byte a = 'A'; a <= 'Z'; a++) {
            int cost = 0;
            for (byte b : s) {
                if (b == a) {
                    continue;
                }
                if (isVowel(a) != isVowel(b)) {
                    cost++;
                } else {
                    cost += 2;
                }
            }
            best = Math.min(best, cost);
        }
        
        out.println(best);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}