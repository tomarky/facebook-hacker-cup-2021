Facebook Hacker Cup 2021に参加したときのコード

Qualification Round	2021/08/29-2021/08/30 64pt 548位 (17:12:41)
  Problem A1: Consistency - Chapter 1       11pt / 11pt
  Problem A2: Consistency - Chapter 2       17pt / 17pt
  Problem B: Xs and Os                      15pt / 15pt
  Problem C1: Gold Mine - Chapter 1         21pt / 21pt
  Problem C2: Gold Mine - Chapter 2          0pt / 36pt
https://www.facebook.com/codingcompetitions/hacker-cup/2021/qualification-round
https://www.facebook.com/codingcompetitions/hacker-cup/2021/qualification-round/scoreboard?start=500


Round 1 2021/09/12 02:00-2021/09/13 02:00 42pt 1353位 (8:26:12)
  Problem A1: Weak Typing - Chapter 1       10pt / 10pt
  Problem A2: Weak Typing - Chapter 2       14pt / 14pt
  Problem A3: Weak Typing - Chapter 3       --pt / 21pt
  Problem B: Traffic Control                18pt / 18pt
  Problem C: Blockchain                      0pt / 37pt
https://www.facebook.com/codingcompetitions/hacker-cup/2021/round-1
https://www.facebook.com/codingcompetitions/hacker-cup/2021/round-1/scoreboard?start=1350


Round 2 2021/09/26 02:00-2021/09/26 05:00 16pt 1688位 (1:50:14)
  Problem A: Runway                         --pt / 12pt
  Problem B: Chainblock                     --pt / 17pt
  Problem C1: Valet Parking - Chapter 1     16pt / 16pt
  Problem C2: Valet Parking - Chapter 2     --pt / 19pt
  Problem D: String Concatenation           --pt / 36pt
https://www.facebook.com/codingcompetitions/hacker-cup/2021/round-2
https://www.facebook.com/codingcompetitions/hacker-cup/2021/round-2/scoreboard

