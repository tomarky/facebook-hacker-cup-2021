// 解法分からず未提出
// 不正解コード
import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        int K = readInt();
        byte[] U = in.readLine().getBytes();
        
        /*
            "."で複製されるため
            複数あれば2倍2倍2倍…と指数関数的にサイズが増えるので
            前問みたいに累積和を持つのは無理
            前問みたいに一方の左端を固定して計算するのも無理
            
            "."は左側からの構築の複製のため
            文字列を右側から走査することがそもそも出来ない…
            (再帰の性質をなんとか利用してバックトラック的に何かできるかもだが)
            FOX..FXOF...FOX.
            
            "."で複製する場合、OとXの繋がりになるとハンドスイッチが増える
            FOXXF.はFOXXF-FOXXF
            FOXOF.はFOXOF-FOXOF
            
            FOX..F
            _OX..F
            __X..F
            ___FOX.F
            ____OX.F
            _____X.F
            ______FOXFOXF
            _______OXFOXF
            ________XFOXF
            _________FOXF
            __________OXF
            ___________XF
            ____________F
        */
        
        long MOD = 1_000_000_007L;
        int cur = -1, first = -1;
        long count = 0L;
        long countSum = 0L;
        long answer = 0L;
        
        for (int i = 0; i < K; i++)
        {
            if (U[i] == 'O')
            {
                if (cur == 1)
                {
                    count = (count + 1) % MOD;
                    answer = (answer + answer + (long)(i + 1)) % MOD;
                }
                cur = 0;
                if (first < 0) first = cur;
            }
            else if (U[i] == 'X')
            {
                if (cur == 0)
                {
                    count = (count + 1) % MOD;
                    answer = (answer + answer + (long)(i + 1)) % MOD;
                }
                cur = 1;
                if (first < 0) first = cur;
            }
            else if (U[i] == '.')
            {
                if (first == cur)
                {
                    count = (count * 2L) % MOD;
                    answer = (answer * 2L + countSum * (long)i % MOD) % MOD;
                    countSum = (countSum + countSum) % MOD;
                }
                else {
                    count = (count * 2L + 1L) % MOD;
                    answer = (answer * 2L + (countSum + 1L) * (long)i % MOD) % MOD;
                    countSum = (countSum + countSum + (long)i) % MOD;
                }
            }
            else
            {
                answer = (answer + answer) % MOD;
            }
            countSum = (countSum + count) % MOD;
        }
        
        out.println(answer);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}