import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        int[] NMAB = readInts();
        int N = NMAB[0];
        int M = NMAB[1];
        int A = NMAB[2];
        int B = NMAB[3];
        int[][] intersec = new int[N][M];
        
        for (int[] row : intersec)
        {
            Arrays.fill(row, 1000);
        }
        
        for (int i = 0; i < M; i++)
        {
            intersec[0][i] = 1;
        }
        
        for (int i = 0; i < N; i++)
        {
            intersec[i][0] = 1;
            intersec[i][M - 1] = 1;
        }
        
        int aa = A - (N + M - 2);
        int bb = B - (N + M - 2);
        
        if (aa < 1 || bb < 1)
        {
            out.println(IMPOSSIBLE);
            return;
        }
        
        intersec[N - 1][M - 1] = aa;
        intersec[N - 1][0] = bb;
        
        out.println(POSSIBLE);
        for (int i = 0; i < N; i++) {
            for (int k = 0; k < M; k++) {
                if (k > 0)
                {
                    out.print(" ");
                }
                out.print(intersec[i][k]);
            }
            out.println();
        }
    }

    static final String POSSIBLE = "Possible";
    static final String IMPOSSIBLE = "Impossible";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}