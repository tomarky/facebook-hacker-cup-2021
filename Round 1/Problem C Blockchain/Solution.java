import java.io.*;
import java.math.*;
import java.util.*;

class Solution
{
    static class Edge
    {
        int from, to, cap;
        Edge(int f, int t, int c)
        {
            from = f; to = t; cap = c;
        }
    }
    
    static ArrayList<ArrayList<Edge>> tree = null;
    static int[][][] DP = null;
    
    static int[] tourFirst(int parent, int cur)
    {
        ArrayList<Edge> list = tree.get(cur);
        int[][] dp = new int[list.size()+1][];
        DP[cur] = dp;
        int[] dpC = new int[21];
        dp[list.size()] = dpC;
        for (int i = 0; i < list.size(); i++)
        {
            Edge e = list.get(i);
            if (e.to == parent) continue;
            int[] tmp1 = tourFirst(cur, e.to);
            int[] tmp2 = new int[21];
            dp[i] = tmp2;
            for (int k = 0; k < 21; k++)
            {
                int z = Math.min(k, e.cap);
                tmp2[z] += tmp1[k];
                dpC[z] += tmp1[k];
            }
            tmp2[e.cap]++;
            dpC[e.cap]++;
        }
        dpC[0]++;
        return dpC;
    }
    
    static void tourSecond(int parent, int cur, int[] tmp1)
    {
        ArrayList<Edge> list = tree.get(cur);
        int[][] dp = DP[cur];
        int[] dpC = dp[list.size()];
        if (tmp1 != null)
        {
            int pi = -1;
            Edge pe = null;
            for (int i = 0; i < list.size(); i++)
            {
                Edge e = list.get(i);
                if (e.to == parent)
                {
                    pi = i;
                    pe = e;
                    break;
                }
            }
            int[] tmp2 = new int[21];
            dp[pi] = tmp2;
            for (int k = 0; k < 21; k++)
            {
                int z = Math.min(k, pe.cap);
                tmp2[z] += tmp1[k];
                dpC[z] += tmp1[k];
            }
            tmp2[pe.cap]++;
            dpC[pe.cap]++;
        }
        int[] tmp = new int[21];
        for (int i = 0; i < list.size(); i++)
        {
            Edge e = list.get(i);
            if (e.to == parent) continue;
            for (int k = 0; k < 21; k++)
            {
                tmp[k] = dpC[k] - dp[i][k];
            }
            tourSecond(cur, e.to, tmp);
        }
    }
    
    static final long MOD = 1_000_000_007L;

    static final long HALF = BigInteger.valueOf(2L)
        .modInverse(BigInteger.valueOf(MOD))
        .longValue();
    
    static long[][] SUM = null;
    static long[][] Answer = null;
    
    static long tourThird(int parent, int cur)
    {
        ArrayList<Edge> list = tree.get(cur);
        long[] sums = new long[list.size()+1];
        Answer[cur] = sums;
        long sumC = SUM[cur][list.size()];
        long minus = 0L;
        for (int i = 0; i < list.size(); i++)
        {
            Edge e = list.get(i);
            if (e.to == parent) {
                minus = SUM[cur][i];
                continue;
            }
            long tmp1 = tourThird(cur, e.to);
            sums[i] = tmp1;
            sumC = (sumC + tmp1) % MOD;
        }
        sums[list.size()] = sumC;
        return (sumC + (MOD - minus)) % MOD;
    }
    
    static void tourForth(int parent, int cur, long value)
    {
        ArrayList<Edge> list = tree.get(cur);
        long[] sums = Answer[cur];
        long sumC = sums[list.size()];
        if (parent >= 0)
        {
            for (int i = 0; i < list.size(); i++)
            {
                Edge e = list.get(i);
                if (e.to != parent) continue;
                sums[i] = value;
                break;
            }
            sumC = (sumC + value) % MOD;
            sums[list.size()] = sumC;
        }
        for (int i = 0; i < list.size(); i++)
        {
            Edge e = list.get(i);
            if (e.to == parent) continue;
            long tmp = sumC + (MOD - sums[i]) + (MOD - SUM[cur][i]);
            tourForth(cur, e.to, tmp % MOD);
        }
    }
    
    static void solve() throws Exception
    {        
        int N = readInt();
        tree = new ArrayList<>(N);
        DP = new int[N][][];
        for (int i = 0; i < N; i++)
        {
            tree.add(new ArrayList<>());
        }
        Edge[] edges = new Edge[N-1];
        for (int i = 0; i < N - 1; i++)
        {
            int[] ABC = readInts();
            int A = ABC[0] - 1;
            int B = ABC[1] - 1;
            int C = ABC[2];
            tree.get(A).add(edges[i] = new Edge(A, B, C));
            tree.get(B).add(new Edge(B, A, C));
        }
        
        tourFirst(-1, 0);
        tourSecond(-1, 0, null);
        
        SUM = new long[N][];
        for (int i = 0; i < N; i++)
        {
            ArrayList<Edge> list = tree.get(i);
            int[][] dp = DP[i];
            int[] dpC = dp[list.size()];
            SUM[i] = new long[list.size() + 1];
            long sumC = 0L;
            for (int k = 0; k < list.size(); k++)
            {
                int[] dpK = dp[k];
                long sumK = 0L;
                for (int x = 1; x < 21; x++)
                {
                    for (int y = 1; y < 21; y++)
                    {
                        long t = (long)dpK[x] * (long)(dpC[y] - dpK[y]) * (long)Math.min(x, y);
                        sumK = (sumK + t) % MOD;
                        sumC = (sumC + t) % MOD;
                    }
                    sumK = (sumK + (long)dpK[x] * (long)x) % MOD;
                }
                SUM[i][k] = sumK;
            }
            sumC = (sumC * HALF) % MOD;
            for (int k = 0; k < list.size(); k++)
            {
                int[] dpK = dp[k];
                for (int x = 1; x < 21; x++)
                    sumC = (sumC + (long)dpK[x] * (long)x) % MOD;
            }
            SUM[i][list.size()] = sumC;
        }
        
        Answer = new long[N][];
        tourThird(-1, 0);
        tourForth(-1, 0, 0L);
        
        long answer = 1L;
        for (Edge e : edges)
        {
            long sumE = 0L;
            ArrayList<Edge> listA = tree.get(e.from);
            for (int i = 0; i < listA.size(); i++)
            {
                Edge ea = listA.get(i);
                if (ea.to != e.to) continue;
                sumE += Answer[e.from][listA.size()]
                    + (MOD - SUM[e.from][i]);
                break;
            }
            // ArrayList<Edge> listB = tree.get(e.to);
            // for (int i = 0; i < listB.size(); i++)
            // {
                // Edge eb = listB.get(i);
                // if (eb.to != e.from) continue;
                // sumE += Answer[e.to][listB.size()]
                    // + (MOD - Answer[e.to][i])
                    // + (MOD - SUM[e.to][i]);
                // break;
            // }
            sumE %= MOD;
            answer = (answer * sumE) % MOD;
        }
        
        out.println(answer);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}