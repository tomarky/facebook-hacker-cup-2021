import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        int N = readInt();
        byte[] S = in.readLine().getBytes();

        /*
            XFOFX F OFXFOFX
            XFOFX F OFXFOF
            XFOFX F OFXFO
            XFOFX F OFXF
            XFOFX F OFX
            XFOFX F OF
            XFOFX F O
            XFOFX F

            XFOFXF O FXFOFX
            XFOFXF O FXFOF
            XFOFXF O FXFO
            XFOFXF O FXF
            XFOFXF O FX
            XFOFXF O F
            XFOFXF O

            XFOFXFO F XFOFX
            XFOFXFO F XFOF
            XFOFXFO F XFO
            XFOFXFO F X
            XFOFXFO F

            XXXOOO F FFOOOXXX 1
            XXXOOO F FFOOOXX  1
            XXXOOO F FFOOOX   1
            XXXOOO F FFOOO    0
            XXXOOO F FFOO     0
            XXXOOO F FFO      0
            XXXOOO F FF       0
            XXXOOO F F        0
            XXXOOO F          0

            XXXOO O FFFOOOXXX 1
            XXXOO O FFFOOOXX  1
            XXXOO O FFFOOOX   1
            XXXOO O FFFOOO    0
            XXXOO O FFFOO     0
            XXXOO O FFFO      0
            XXXOO O FFF       0
            XXXOO O FF        0
            XXXOO O F         0
            XXXOO O           0

            XXXOOO F FFXXXOOO 1 (2)
            XXXOOO F FFXXXOO  1 (2)
            XXXOOO F FFXXXO   1 (2)
            XXXOOO F FFXXX    0 (1)
            XXXOOO F FFXX     0 (1)
            XXXOOO F FFX      0 (1)
            XXXOOO F FF       0
            XXXOOO F F        0
            XXXOOO F          0

            XXXOO O FFFXXXOOO 2
            XXXOO O FFFXXXOO  2
            XXXOO O FFFXXXO   2
            XXXOO O FFFXXX    1
            XXXOO O FFFXX     1
            XXXOO O FFFX      1
            XXXOO O FFF       0
            XXXOO O FF        0
            XXXOO O F         0
            XXXOO O           0

            X FO   1
            X F_   0
            X __   0
            _ F O  0 (1)
            _ F _  0
            __ O   0
        */

        long[] left = new long[N + 1];
        int[] area = new int[N];

        int cur = -1;
        for (int i = 0; i < N; i++)
        {
            left[i+1] = left[i];
            if (S[i] == 'O')
            {
                if (cur == 1)
                {
                    left[i+1]++;
                }
                cur = 0;
            }
            else if (S[i] == 'X')
            {
                if (cur == 0)
                {
                    left[i+1]++;
                }
                cur = 1;
            }
            area[i] = cur;
        }

        long[] fs = new long[N + 1];
        long[] sum = new long[N + 1];
        cur = -1;
        for (int i = N; i > 0; i--) {
            sum[i - 1] = sum[i] + left[i];
            if (S[i-1] == 'O')
            {
                cur = 0;
            }
            else if (S[i-1] == 'X')
            {
                cur = 1;
            }
            if (area[i-1] != cur)
            {
                if (area[i-1] < 0 || cur < 0)
                {
                    area[i-1] = -1;
                }
                else
                {
                    area[i-1] = -2;
                }
            }
            if (S[i-1] == 'F')
            {
                fs[i-1] += fs[i] + 1;
            }
        }

        long answer = 0L;
        for (int i = 0; i < N; i++)
        {
            long tmp = (long)(N - i) * left[i + 1] % 1_000_000_007L;
            answer = (answer + 1_000_000_007L - tmp) % 1_000_000_007L;
            answer = (answer + sum[i]) % 1_000_000_007L;
            if (area[i] == -2)
            {
                answer = (answer + 1_000_000_007L - (long)(N - i - fs[i])) % 1_000_000_007L;
            }
        }

        out.println(answer);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}