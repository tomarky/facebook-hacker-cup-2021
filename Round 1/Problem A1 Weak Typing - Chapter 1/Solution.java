import java.io.*;
import java.util.*;

class Solution
{
    static void solve() throws Exception
    {
        // solution
        int N = readInt();
        byte[] W = in.readLine().getBytes();

        int answer = 0;

        int cur = -1;

        for (int i = 0; i < N; i++)
        {
            if (W[i] == 'O') {
                if (cur == 1) {
                    answer++;
                }
                cur = 0;
            } else if (W[i] == 'X') {
                if (cur == 0) {
                    answer++;
                }
                cur = 1;
            }
        }

        out.println(answer);
    }

    static final String POSSIBLE = "POSSIBLE";
    static final String IMPOSSIBLE = "IMPOSSIBLE";
    static BufferedReader in;
    static PrintStream out = System.out;
    static PrintStream err = System.err;

    public static void main(String[] args) throws Exception
    {
        in = new BufferedReader(new InputStreamReader(System.in));

        int T = readInt();
        for (int i = 1; i <= T; i++)
        {
            out.printf("Case #%d: ", i);
            solve();
        }
    }

    static int readInt() throws Exception
    {
        return Integer.parseInt(in.readLine());
    }

    static int[] readInts() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        int[] ret = new int[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Integer.parseInt(tokens[i]);
        }
        return ret;
    }

    static long[] readLongs() throws Exception
    {
        String[] tokens = in.readLine().split(" ");
        long[] ret = new long[tokens.length];
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = Long.parseLong(tokens[i]);
        }
        return ret;
    }
}